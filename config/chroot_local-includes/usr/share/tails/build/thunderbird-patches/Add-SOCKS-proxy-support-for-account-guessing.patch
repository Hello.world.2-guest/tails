From bb7b4741004c367132869b56dbd62a829ac67167 Mon Sep 17 00:00:00 2001
From: anonym <anonym@riseup.net>
Date: Wed, 27 Feb 2019 09:54:59 +0100
Subject: [PATCH] Add SOCKS proxy support for account guessing.

Any configured SOCKS proxy will be used while probing servers, but
HTTP(s) proxies etc will be ignored since they are not
applicable. This solves Mozilla bug #669238:

    https://bugzilla.mozilla.org/show_bug.cgi?id=669238

Refreshed-by: Cyril Brulebois <ckb@riseup.net>

Backported from TB 66 to TB 65, dropping reindentation to have a
higher chance of applying this patch successfully against further
65.x releases.

--- a/comm/mail/components/accountcreation/content/guessConfig.js
+++ b/comm/mail/components/accountcreation/content/guessConfig.js
@@ -467,9 +467,18 @@ HostDetector.prototype =
       if (i == 0) // showing 50 servers at once is pointless
         this.mProgressCallback(thisTry);
 
+      // This implements the nsIProtocolProxyCallback interface:
+      function ProxyResolveCallback() { }
+      ProxyResolveCallback.prototype = {
+        onProxyAvailable : function(req, uri, proxyInfo, status) {
+          // Anything but a SOCKS proxy will be unusable for the probes.
+          if (proxyInfo != null && proxyInfo.type != "socks" &&
+              proxyInfo.type != "socks4") {
+            proxyInfo = null;
+          }
       thisTry.abortable = SocketUtil(
           thisTry.hostname, thisTry.port, thisTry.ssl,
-          thisTry.commands, TIMEOUT,
+          thisTry.commands, TIMEOUT, proxyInfo,
           new SSLErrorHandler(thisTry, this._log),
           function(wiredata) // result callback
           {
@@ -487,6 +496,21 @@ HostDetector.prototype =
             thisTry.status = kFailed;
             me._checkFinished();
           });
+        }
+      };
+
+      var proxyService = Cc["@mozilla.org/network/protocol-proxy-service;1"]
+                         .getService(Ci.nsIProtocolProxyService);
+      // Use some arbitrary scheme just because it is required...
+      var uri = Services.io.newURI("http://" + thisTry.hostname, null, null);
+      // ... we'll ignore it any way. We prefer SOCKS since that's the
+      // only thing we can use for email protocols.
+      var proxyFlags = Ci.nsIProtocolProxyService.RESOLVE_IGNORE_URI_SCHEME |
+                       Ci.nsIProtocolProxyService.RESOLVE_PREFER_SOCKS_PROXY;
+      if (Services.prefs.getBoolPref("network.proxy.socks_remote_dns")) {
+        proxyFlags |= Ci.nsIProtocolProxyService.RESOLVE_ALWAYS_TUNNEL;
+      }
+      proxyService.asyncResolve(uri, proxyFlags, new ProxyResolveCallback());
       thisTry.status = kOngoing;
     }
   },
@@ -1019,13 +1043,14 @@ SSLErrorHandler.prototype =
  * @param commands {Array of String}: protocol commands
  *          to send to the server.
  * @param timeout {Integer} seconds to wait for a server response, then cancel.
+ * @param proxy {nsIProxyInfo} The proxy to use (or null to not use any).
  * @param sslErrorHandler {SSLErrorHandler}
  * @param resultCallback {function(wiredata)} This function will
  *            be called with the result string array from the server
  *            or null if no communication occurred.
  * @param errorCallback {function(e)}
  */
-function SocketUtil(hostname, port, ssl, commands, timeout,
+function SocketUtil(hostname, port, ssl, commands, timeout, proxy,
                     sslErrorHandler, resultCallback, errorCallback)
 {
   assert(commands && commands.length, "need commands");
@@ -1064,7 +1089,7 @@ function SocketUtil(hostname, port, ssl,
   var socketTypeName = ssl == SSL ? "ssl" : (ssl == TLS ? "starttls" : null);
   var transport = transportService.createTransport([socketTypeName],
                                                    ssl == NONE ? 0 : 1,
-                                                   hostname, port, null);
+                                                   hostname, port, proxy);
 
   transport.setTimeout(Ci.nsISocketTransport.TIMEOUT_CONNECT, timeout);
   transport.setTimeout(Ci.nsISocketTransport.TIMEOUT_READ_WRITE, timeout);
