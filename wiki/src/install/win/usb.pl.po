# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-01-07 18:02+0000\n"
"PO-Revision-Date: 2018-10-30 07:41+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 2.19.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Install from Windows\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!meta robots=\"noindex\"]]\n"
msgstr "[[!meta robots=\"noindex\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"bootstrap.min\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"bootstrap.min\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"inc/stylesheets/assistant\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"inc/stylesheets/steps\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr "[[!meta stylesheet=\"inc/stylesheets/steps\" rel=\"stylesheet\" title=\"\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta stylesheet=\"inc/stylesheets/windows\" rel=\"stylesheet\" title=\"\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"hidden-step-1\"></div>\n"
msgstr "<div class=\"hidden-step-1\"></div>\n"

#. type: Plain text
msgid "These instructions require:"
msgstr ""

#. type: Bullet: '  - '
msgid "Windows 7 (64-bit) or later"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"step-image\">[[!img install/inc/infography/os-windows.png link=\"no\" alt=\"\"]]</div>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<p class=\"start\">Start in Windows.</p>\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Install Tails using <span class=\"application\">Etcher</span>\n"
msgstr ""

#. type: Bullet: '1. '
msgid ""
"Click on the following link to download <span class=\"application\">Etcher</"
"span>:"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "   <a href='https://tails.boum.org/etcher/Etcher-Portable.exe'>Download Etcher for Windows</a>\n"
msgstr ""

#. type: Plain text
#, fuzzy, no-wrap
msgid "[[!inline pages=\"install/inc/steps/install_with_etcher.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/install_final.inline.pl\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/restart_first_time.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/restart_first_time.inline.pl\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"install/inc/steps/create_persistence.inline\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"install/inc/steps/create_persistence.inline.pl\" raw=\"yes\" sort=\"age\"]]\n"
