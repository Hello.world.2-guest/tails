# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-05-14 16:29+0000\n"
"PO-Revision-Date: 2018-07-02 06:16+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Upgrading a Tails USB stick\"]]\n"
msgstr ""

#. type: Plain text
msgid ""
"Tails includes an automatic mechanism to upgrade a USB stick to a newer "
"version. In some cases, it is impossible to do an **automatic upgrade** and "
"you might have to do a **manual upgrade**. This page describes both "
"techniques."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
msgid ""
"Our upgrades always fix important security issues so it is important to do "
"them as soon as possible."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Plain text
#, no-wrap
msgid "<div class=\"note\">\n"
msgstr "<div class=\"note\">\n"

#. type: Plain text
#, no-wrap
msgid "<p>The persistent storage on the USB stick will be preserved.</p>\n"
msgstr ""

#. type: Plain text
msgid ""
"If you use Tails from a DVD, you need to [[burn a new DVD|install/dvd]]."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=2]]\n"
msgstr "[[!toc levels=2]]\n"

#. type: Plain text
#, no-wrap
msgid "<a name=\"automatic\"></a>\n"
msgstr "<a name=\"automatic\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Automatic upgrade using <span class=\"application\">Tails Upgrader</span>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"After starting Tails and connecting to Tor, <span class=\"application\">Tails\n"
"Upgrader</span> automatically checks if upgrades are available and then\n"
"proposes you to upgrade your USB stick. The upgrades are checked for and downloaded\n"
"through Tor.\n"
msgstr ""

#. type: Plain text
msgid "The advantages of this technique are the following:"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"You only need a single Tails USB stick. The upgrade is done on the fly from "
"a running Tails. After upgrading, you can restart and use the new version."
msgstr ""

#. type: Bullet: '  - '
msgid "The upgrade is much smaller to download than a full USB image."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"The upgrade mechanism includes cryptographic verification of the upgrade.  "
"You don't have to verify the USB image yourself anymore."
msgstr ""

#. type: Plain text
msgid "Requirements:"
msgstr ""

#. type: Bullet: '  - '
msgid "A Tails USB stick."
msgstr ""

#. type: Bullet: '  - '
msgid "An Internet connection."
msgstr ""

#. type: Plain text
msgid ""
"After connecting to Tor, if an upgrade is available, a dialog appears and "
"proposes you to upgrade your USB stick."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img upgrader_automatic.png link=no]]\n"
msgstr "[[!img upgrader_automatic.png link=no]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"<ul>\n"
"<li>We recommend you close all other applications during the upgrade.</li>\n"
"<li>Downloading the upgrade might take a long time, from several minutes to a\n"
"few hours.</li>\n"
"<li>The networking will be disabled after downloading the upgrade.</li>\n"
"</ul>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"If you decide to do the upgrade, click on <span\n"
"class=\"guilabel\">Upgrade now</span>,\n"
"and follow the assistant through the upgrade process.\n"
msgstr ""

#. type: Plain text
msgid ""
"If you missed an upgrade, each upgrade will be installed one after the "
"other. For example, if you have a Tails 1.3 and the current version is "
"1.3.2, then the upgrade to 1.3.1 will be installed, and after you restart "
"Tails, the upgrade to 1.3.2 will be installed."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"tip\">\n"
msgstr "<div class=\"tip\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>If you cannot upgrade at startup (for example if you have no network\n"
"connection by then), you can start <span class=\"application\">Tails\n"
"Upgrader</span> later by opening a terminal and executing the following\n"
"command:</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<pre>tails-upgrade-frontend-wrapper</pre>\n"
msgstr "<pre>tails-upgrade-frontend-wrapper</pre>\n"

#. type: Plain text
#, no-wrap
msgid "<p>We recommend you read the [[release notes|release_notes]] for the latest version. They document all the changes in this new version:</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<ul>\n"
"  <li>new features</li>\n"
"  <li>problems that were solved</li>\n"
"  <li>known issues that have already been identified</li>\n"
"</ul>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<p>They might also contain <strong>special instructions for upgrading</strong>.</p>\n"
msgstr ""

#. type: Plain text
msgid ""
"If an error occurs, the assistant proposes you to read one of the following "
"pages:"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"If an error occurs while checking for available upgrades:<br/> [[file:///usr/"
"share/doc/tails/website/doc/upgrade/error/check.en.html|upgrade/error/check]]"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"If an error occurs while download the upgrade:<br/> [[file:///usr/share/doc/"
"tails/website/doc/upgrade/error/download.en.html|upgrade/error/download]]"
msgstr ""

#. type: Bullet: '  - '
msgid ""
"If an error occurs while installing the upgrade:<br/> [[file:///usr/share/"
"doc/tails/website/doc/upgrade/error/install.en.html|upgrade/error/install]]"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<a name=\"manual\"></a>\n"
msgstr "<a name=\"manual\"></a>\n"

#. type: Title =
#, no-wrap
msgid "Manual upgrade using <span class=\"application\">Tails Installer</span>\n"
msgstr ""

#. type: Plain text
msgid ""
"It might not always be possible to do an automatic upgrade as described "
"above.  For example, when:"
msgstr ""

#. type: Bullet: '  - '
msgid "No automatic upgrade is available from our website for this version."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"The automatic upgrade is impossible for technical reasons (not enough "
"memory, not enough free space on the USB stick, etc.)."
msgstr ""

#. type: Bullet: '  - '
msgid ""
"You want to upgrade from another Tails USB stick which already has a newer "
"version installed, for example when working offline."
msgstr ""

#. type: Bullet: '  - '
msgid "The automatic upgrade failed and you need to repair a Tails USB stick."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"After connecting to Tor, a dialog informs you if you have to\n"
"upgrade your USB stick using <span class=\"application\">Tails Installer</span>\n"
"to a newer version of Tails.\n"
"To do so, follow our [[manual upgrade instructions|/upgrade]].\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img upgrader_manual.png link=no]]\n"
msgstr "[[!img upgrader_manual.png link=no]]\n"

#. type: Plain text
#, no-wrap
msgid ""
"To know your version of Tails, choose\n"
"<span class=\"menuchoice\">\n"
"  <span class=\"guimenu\">Applications</span>&nbsp;▸\n"
"  <span class=\"guisubmenu\">Tails</span>&nbsp;▸\n"
"  <span class=\"guimenuitem\">About Tails</span>\n"
"</span>\n"
msgstr ""
